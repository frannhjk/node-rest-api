const jwt = require('jsonwebtoken');
const config = require('../config/config.json');

// Default middleware pattern for Express
module.exports = (req, res, next) => {
    try{
        const token = req.headers.authorization.split(" ")[1]; // Remove 'Bearer'
        //console.log(token);
        const decoded = jwt.verify(token, config.Development.JWT_KEY);
        req.userData = decoded;
        next();
    }
    catch(error){
        return res.status(401).json({
            message: 'Auth failed'
        })
    }
};