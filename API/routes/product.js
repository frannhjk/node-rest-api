const express = require('express');
const router = express.Router();
const multer = require('multer');
const path = require('path');
const checkAuth = require('../middleware/check-auth');

//#region Upload Image
var __dirname = './uploadFilesAca/';

var storage = multer.diskStorage({   
    destination: function(req, file, cb) { 
        cb(null, __dirname);
    }, 
    filename: function (req, file, cb) { 
       cb(null , file.originalname);   
    }
 });

 const fileFilter = (req, file, cb) => {
    // Reject a file
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png'){ 
        // Accept a file
        cb(null, true);
    }
    else{
        cb(null, false);
    }
 };

// Initialize multer
const upload = multer({ 
    storage: storage, 
    limits: {
        // max 5 mb
        fileSize: 1024 * 1024 * 5
    },
    fileFilter: fileFilter
});

//#endregion

// Import Model
const Product = require('../models/product');
const { request } = require('../../app');
const { restart } = require('nodemon');

const ProductController = require('../controllers/products');


// No pongo /productos porque todo lo que sea producto lo redireccionaré a este js mediante el middleware

// GetAll
router.get('/', ProductController.products_getAll);

// GetById
router.get('/:productId', ProductController.products_getOne);

// Post
router.post('/', checkAuth, upload.single('productImage'), ProductController.products_post);

// Patch
router.patch('/:productId', checkAuth, ProductController.products_patchProduct);

// Delete
router.delete('/:productId', checkAuth, ProductController.products_delete);


module.exports = router; 