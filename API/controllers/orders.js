const mongoose = require('mongoose');

const Order = require('../models/order');
const Product = require('../models/product');


exports.orders_getAll = (req, res, next) => {

    Order.find()
        .select('product quantity _id')
        // Mostrar información del producto además de las Orders
        .populate('product', 'name')
        .exec()
        .then(docs => {
            res.status(200).json({
                count: docs.length,
                orders: docs.map(doc => {
                    return {
                        _id: doc._id,
                        product: doc.product,
                        quantity: doc.quantity,
                        resquest:{
                            type: 'GET',
                            url: 'http://' + req.headers.host + '/orders/' + doc._id
                        }
                    }
                })
            });
        })
        .catch(err => {
            res.status(500).json({
                error: err
            })
        });
};

exports.orders_post = (req, res, next) => {

    Product.findById(req.body.productId)
        .then(product => {

            if (!product){
                return res.status(404).json({
                    message: 'Product not found'
                });
            }

            const order = new Order({
                _id: mongoose.Types.ObjectId(), 
                quantity: req.body.quantity,
                product: req.body.productId
            });
        
            return order
            .save()
        })
        .then(result => {
            console.log(result);
            res.status(201).json({
                message: 'Order stored',
                createdOrder: {
                    _id: result._id,
                    product: result.product,
                    quantity: result.quantity   
                },
                request: {
                    type: 'GET',
                    url: 'http://' + req.headers.host + '/orders/' + result._id
                }
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            })
        });
};

exports.orders_delete = (req, res, next) => {

    const id = req.params.orderId;

    Order.remove({_id: id})
        .exec()
        .then(result => {
            res.status(200).json({
                message: 'Order deleted',
                request:{
                    type: 'DELETE'
                }
            });
        })
        .catch(err =>{
            res.status(500).json({
                error: err
            })
        });
};


exports.orders_getOne = (req, res, next) => {
    Order.findById(req.params.orderId)
        .populate('product')
        .exec()
        .then(doc => {
            if (!doc){
                return res.status(404).json({
                    message: 'Order not found'
                });
            }
            res.status(200).json({
                order: doc,
                request: {
                    type: 'GET',
                    url: 'http://' + req.headers.host + '/orders/'
                }
            })
        })
        .catch(err => {
            res.status(500).json({
                error: err
            })
        });
};