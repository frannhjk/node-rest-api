//#region Imports - Requires
const express = require('express');
const app = express();
const morgan = require('morgan');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
//#endregion


// Import Routes
const productRoute = require('./API/routes/product');
const orderRoute = require('./API/routes/order');
const userRoute = require('./API/routes/user');


// DB Connection
mongoose.connect('mongodb+srv://shop-rest:shop-rest@cluster0.y7xyp.mongodb.net/<dbname>?retryWrites=true&w=majority', {
});   

mongoose.Promise = global.Promise;


// Public uploads folder
app.use('/uploadFilesAca', express.static('uploadFilesAca'));


// Morgan for logging
app.use(morgan('dev'));


// Body parser for post requests
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());


// Handling CORS by Header
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*"); 
    res.header("Access-Control-Allow-Headers, Origin, X-Requested-With, Content-Type, Accept, Authorization");
    
    if (req.method === 'OPTIONS')
    {
        req.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET');
        res.status(200).json({});
    }
    next();
});


// Middleware (toda la request de /products) utilizará el handler correspondiente
app.use('/products', productRoute);
app.use('/orders', orderRoute);
app.use('/user', userRoute);


//#region Error Handling 
app.use((req, res, next) =>{
    const error = new Error('Not found');
    error.status = 404;
    next(error);
});

app.use((error, req, res, next) => {
    res.status(error.status || 500);
    
    res.json({
        error: {
            message: error.message
        }
    });
});
//#endregion

module.exports = app;