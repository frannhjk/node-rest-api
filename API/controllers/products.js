const mongoose = require('mongoose');

const Product = require('../models/product');


exports.products_getAll = (req, res, next) => {
    Product
        .find()
        .select('name price _id productImage')
        .exec()
        .then(doc => {
            const response = {
                count: doc.length,
                products: doc.map(doc => {
                    return {
                        name: doc.name,
                        price: doc.price,
                        _id: doc._id,
                        productImage: doc.productImage,
                        request:{
                            type: 'GET',
                            url: 'http://' + req.headers.host + '/products/' + doc._id
                        }
                    }
                })
            };
            res.status(200).json(response)
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
};


exports.products_getOne = (req, res, next) => {
    
    const id = req.params.productId;

    Product.findById(id)
        .select('name price _id productImage')
        .exec()
        .then(doc => {
            if (doc){
                res.status(200).json({
                    name: doc.name,
                    price: doc.price,
                    _id: doc._id,
                    productImage: doc.productImage,
                    request: {
                        type: 'GET/{id}',
                        description: 'Get all products',
                        url: 'http://' + req.headers.host + '/products/'
                    }
                })
            }
            else{
                res.status(404).json({
                    message: "No valid entry found for provided Product ID"
                });
            }
            res.status(200).json(doc);
        })
        .catch(err => {
            console.log(err);
            res.status(404).json({error: err});
        });
};

exports.products_post = (req, res, next) => {

    //console.log(req.file);

    // Creo un Producto
    const product = new Product({
        _id: new mongoose.Types.ObjectId(),
        name: req.body.name,
        price: req.body.price,
        productImage: req.file.path
    });

    // Lo guardo
    product
        .save()
        .then(result => {
            console.log(result);
            res.status(201).json({
                message: "Product created succesfully",
                createdProduct: {
                    name: result.name,
                    price: result.price,
                    _id: result._id,
                    request: {
                        type: 'GET',
                        url: 'http://' + req.headers.host + '/products/' + result._id
                    }
                }
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            })
        });
};

exports.products_patchProduct = (req, res, next) => {
    
    const id = req.params.productId;

    const updateOperations = {};

    // Para que pueda hacer un Update tanto de un atributo como de todos (patch), debe ser propName
    for (const ops of req.body){
        updateOperations[ops.propName] = ops.value;
    }

    // Update objecto que coincida con el ID enviado en el request
    //Product.update({_id: id}, { $set: { name: req.body.newName, price: req.body.newPrice } }) 
    Product.update({_id: id}, { $set: updateOperations }) 
        .exec()
        .then(result =>{
            console.log(result);
            res.status(200).json({
                message: 'Product updated',
                request: {
                    type: 'PATCH',
                    url: 'http://' + req.headers.host + '/products/' + result._id
                }
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
};

exports.products_delete = (req, res, next) => {

    const id = req.params.productId;

    // Remove objeto que coincida con el ID enviado en el request 
    Product.remove({_id: id}) 
        .exec()
        .then(result =>{
            res.status(200).json({
                message: "Product deleted",
                request:{
                    type: 'DELETE'
                }
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            }); 
        });
};